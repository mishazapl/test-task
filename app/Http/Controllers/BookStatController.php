<?php

namespace App\Http\Controllers;


use App\Services\Book\BookService;
use Illuminate\Http\Request;

class BookStatController extends Controller
{

    /**
     * @param Request $request
     * @param BookService $service
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function index(Request $request, BookService $service)
    {
        $this->validate($request, [
            'period_start' => 'required|date:Y-m',
            'period_end' => 'required|date:Y-m',
        ]);

        $period = [$request->get('period_start'), $request->get('period_end')];

        return response()->json(['stat' => $service->getStat($period)],200);
    }
}
