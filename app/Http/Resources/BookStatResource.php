<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use function Sodium\add;

class BookStatResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $response = [];

        if (!is_null($this->date)) {
            $response['date'] = $this->date;
        }

        $response['value'] = $this->sales;

        if (!is_null($this->book)) {
            $response['title'] = $this->book->title;
        }

        return $response;
    }
}
