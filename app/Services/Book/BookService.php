<?php
/**
 * Created by PhpStorm.
 * User: mihail
 * Date: 8/12/19
 * Time: 11:30 AM
 */

namespace App\Services\Book;


use App\BookStat;
use App\Http\Resources\BookStatResource;
use Carbon\Carbon;

class BookService
{
    private function convertDate(array $period)
    {
        return ["{$period[0]}-01", "{$period[1]}-01"];
    }

    /**
     * @param array $period
     * @return array
     */
    public function getStat(array $period)
    {
        $period = $this->convertDate($period);
        $years  = $this->getYears($period);
        $stats  = $this->getStats($period, $years);

        return $this->getResponse($stats);
    }

    /**
     * @param $stats
     * @return array
     */
    private function getResponse($stats): array
    {
        $response = [];

        foreach ($stats as $stat) {
            $response[] = new BookStatResource($stat);
        }

        return $response;
    }

    /**
     * @param array $period
     * @param array $years
     * @return BookStat[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private function getStats(array $period, array $years)
    {
        return BookStat::with('book')
            ->period($period)
            ->years($years)
            ->allStat()
            ->select('book_id', 'period', 'sales', 'year')
            ->groupBy('period', 'book_id', 'sales', 'year')
            ->orderBy('period')
            ->get();
    }

    /**
     * @param array $period
     * @return array
     */
    private function getYears(array $period): array
    {
        $years = [];
        $yearStart = Carbon::parse($period[0])->year;
        $yearEnd = Carbon::parse($period[1])->year;

        for ($start = $yearStart; $start <= $yearEnd; $start++) {
            $years[] = $start;
        }

        return $years;
    }
}