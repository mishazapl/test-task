<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class BookStat
 * @package App
 *
 * @method Builder|BookStat period(array $period) static
 * @method Builder|BookStat years(array $years) static
 * @method Builder|BookStat allStat() static
 */
class BookStat extends Model
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'book_id',
        'period',
        'sales',
        'year'
    ];

    /**
     * @return null|string
     */
    public function getDateAttribute()
    {
        if ($this->isYear()) {
            return Carbon::parse($this->period)->format('Y');
        }

        if ($this->isForAllTime()) {
            return null;
        }

        return Carbon::parse($this->period)->format('Y-m');
    }

    /**
     * @return bool
     */
    public function isYear(): bool
    {
        return is_null($this->book_id) && !is_null('period') && !is_null($this->year);
    }

    /**
     * @return bool
     */
    public function isForAllTime(): bool
    {
        return is_null($this->book_id) && is_null($this->period) && is_null($this->year);
    }

    /**
     * @return HasOne
     */
    public function book(): HasOne
    {
        return $this->hasOne(Book::class, 'id', 'book_id');
    }

    /**
     * @param Builder $builder
     * @param array $period
     * @return Builder
     */
    public function scopePeriod(Builder $builder, array $period): Builder
    {
        return $builder->whereBetween('period', $period);
    }

    /**
     * @param Builder $builder
     * @param array $years
     * @return Builder
     */
    public function scopeYears(Builder $builder, array $years): Builder
    {
        return $builder->orWhereIn('year', $years);
    }

    /**
     * @param Builder $builder
     * @return Builder
     */
    public function scopeAllStat(Builder $builder): Builder
    {
        return $builder->orWhere(function ($query) {
            $query->whereNull('period')
                ->whereNull('year')
                ->whereNull('book_id');
        });
    }
}
