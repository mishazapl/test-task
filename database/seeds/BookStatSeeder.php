<?php

use Illuminate\Database\Seeder;

class BookStatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\BookStat::class, 100)
            ->create();
        $this->createByMonth();
        $this->createByYear();
        $this->allTimeStat();
    }

    private function createByMonth()
    {
        $stats = \App\BookStat::selectRaw('sum(sales) as sale, period')
            ->groupBy('period')
            ->get();

        foreach ($stats as $stat) {

            \App\BookStat::create([
                'period' => $stat->period,
                'sales'  => $stat->sale
            ]);

        }
    }

    private function createByYear()
    {
        $years = [2017,2018,2019];

        foreach ($years as $year) {

            $stats = \App\BookStat::selectRaw('sum(sales) as sale')
                ->groupBy('period')
                ->whereNull('book_id')
                ->whereRaw("extract(year from period) = {$year}")
                ->get()
                ->sum('sale');

            \App\BookStat::create([
                'period' => "{$year}-12-31",
                'sales'  => $stats,
                'year'   => $year
            ]);

        }
    }

    private function allTimeStat()
    {
        \App\BookStat::create([
            'sales'  => \App\BookStat::sum('sales')
        ]);
    }
}
