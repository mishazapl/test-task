<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Book;
use Faker\Generator as Faker;

$factory->define(\App\BookStat::class, function (Faker $faker) {
    return [
        'book_id' => Book::inRandomOrder()->first()->id,
        'period' => rand(2017,2019) . '-' . $faker->month . '-' . '01',
        'sales' => rand(0, 100)
    ];
});

