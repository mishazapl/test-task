<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BookStatHttpTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testIndexPage()
    {
        $response = $this->get('/api/books/stat?period_start=2018-03&period_end=2019-03');
        $response->assertStatus(200);
    }
}
